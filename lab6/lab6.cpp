#include <iostream>
#include <fstream>
#include <ctime>
using namespace std;

double f(double(&x)[200][4], double* w, int num);
double MSE(double(&)[200][2]); 
double Calibration(double(&x)[200][4], double(&y)[200][2], double* w, int i);

int main()
{
	srand(time(NULL));
	double y[200][2], x[200][4], w[4], err = pow(10, 10);
	ifstream X, Y;
	X.open("x.csv");
	Y.open("y.csv");
	for (int num = 0; num < 200; num++)
	{
		x[num][0] = 1;
		for (int i = 1; i < 4; i++)
		{
			X >> x[num][i];
		}
		Y >> y[num][0];
	}
	X.close();
	Y.close();
	do {
		for (int i = 0; i < 4; i++)	w[i] = rand() % 1000 - 500;
		for (int fail = 0; fail < 500; fail++)
		{
			int i = rand() % 4;
			err = Calibration(x, y, w, i);
		}
	} while (err > pow(10, 6));

	return 0;
}
double f(double(&x)[200][4], double* w, int num)
{
	double sum = 0;
	for (int i = 0; i < 4; i++)	sum += x[num][i] * w[i];
	return sum;
}
double MSE(double(&y)[200][2])
{
	double sum = 0;
	for (int i = 0; i < 200; i++) sum += pow(y[i][0] - y[i][1], 2);
	return sum / 200;
}
double Calibration(double(&x)[200][4], double(&y)[200][2], double* w, int i)
{
	double err;
	for (int course = 0; course < 2;)
	{
		for (int st = 0; st >= -3;)
		{
			err = MSE(y);
			if (course)
				w[i] += pow(10, st);
			else
				w[i] -= pow(10, st);
			for (int num = 0; num < 200; num++)
				y[num][1] = f(x, w, num);
			if (err <= MSE(y))
			{
				if (course)
					w[i] -= pow(10, st);
				else
					w[i] += pow(10, st);
				st--;
			}
		}
		course++;
	}
	cout << "ERROR = " << err << "      ";
	for (int i = 0; i < 4; i++)
		cout << "w" << i << " = " << w[i] << "  ";
	cout << endl;
	return err;
}
