
//������� 8
//SC = U(m, 6), R = 10 - E(0.5), HA = N(6,0.5), AP = N(25,5.5), A = N(AP,5.5), M = N(10,1.5), T = N(3,1.5), I = U(20,20), 
//RT = 1+U(20-1.5*HA-0.5*M, 5-0.25*HA-0.1*M), DR = A*0.05 + U(15-1.5*HA-0.5*M, 3-0.25*HA-0.1*M)

#include <iostream>
#include <vector>
using namespace std;

struct infected
{
	int m;
	float SC, R, HA, RT, DR, M, A;
	int condition_of_agent;
	float chanceToStartTreatment;
	int dayOfRecovering;
};
enum StatusOfAgent { infectedAndKnowIt, infectedAndDontKnow, isTreated };

void newInfected(vector<infected>& people, int infectedToday, int day);

float N(float m, float sigma);
float U(float c, float d);
float E(float lamda);
bool chance(float chance);

float AP = N(25, 5.5);

int main()
{
	setlocale(0, "");

	int infectedInOneDay[50];

	const int infectedAtTheSameTime = 3000;

	int all_recovered = 0, all_infected = 0, all_dead = 0;
	int infected_today = 5 + rand() % 6;
	float T = N(3, 1.5), I = U(20, 20);
	float SC = 0;
	vector<infected> people;

	bool noMoreInfected = 0;

	int day = 0;
	for (day; day < 50; day++)
	{
		infectedInOneDay[day] = infected_today;
		if (!noMoreInfected)
		{
			newInfected(people, infected_today, day);
			all_infected += infected_today;
		}

		cout << "����  " << day + 1 << "\n��� ���������: " << all_infected << "(+" << infected_today <<
			")  ��� �������������: " << all_recovered << "  ��� �������: " << all_dead << endl;

		infected_today = 0;

		//������� �������� + ������� ���������� ���������
		SC = 0;
		for (int i = 0; i < people.size(); i++)
		{
			if (people[i].condition_of_agent == infectedAndDontKnow)
			{
				if (chance(T))
				{
					people[i].condition_of_agent = infectedAndKnowIt;
					people[i].SC = U(people[i].m, 6) / people[i].R;
					if (people[i].HA < 2) people[i].chanceToStartTreatment = 0.1 * 100;
					else people[i].chanceToStartTreatment = 0.01 * 100;
				}
			}
			if (people[i].condition_of_agent != isTreated)
			{
				if (chance(people[i].chanceToStartTreatment))
				{
					people[i].condition_of_agent = isTreated;
					people[i].SC = 0;
					people[i].M = N(10, 1.5);
					people[i].DR = people[i].A * 0.05 + U(15 - 1.5 * people[i].HA - 0.5 * people[i].M, 3 - 0.25 * people[i].HA - 0.1 * people[i].M);
					people[i].RT = 1 + U(20 - 1.5 * people[i].HA - 0.5 * people[i].M, 5 - 0.25 * people[i].HA - 0.1 * people[i].M);
					people[i].dayOfRecovering += people[i].RT;
				}
			}
			if (people[i].condition_of_agent == infectedAndDontKnow) people[i].SC = U(people[i].m, 6);
			SC += people[i].SC;
		}
		//

		//����e �������/�������������
		for (int i = 0; i < people.size();)
		{

			if (day == people[i].dayOfRecovering)
			{
				all_recovered++;
				people.erase(people.begin() + i);
			}
			else if (chance(people[i].DR))
			{
				all_dead++;
				people.erase(people.begin() + i);
			}
			else ++i;
		}
		//

		//���-�� ����� ���������
		for (int i = 0; i < SC; i++)
		{
			if (chance(I)) infected_today++;
		}
		//

		if (people.size() + infected_today >= infectedAtTheSameTime)
		{
			infected_today = infectedAtTheSameTime - people.size();
			all_infected += infected_today;
			noMoreInfected = 1;
			newInfected(people, infected_today, day);
		}
		if (people.size() == 0) break;

	}
	cout << "________________________________________________________________________________________________________________" << endl << endl;
	people.clear();
	int m = all_infected / day;
	cout << "���. ��������: " << m << endl;
	cout << "����������� ����������: ";
	float a = (float)1 / 49;
	float b = 0;
	for (int i = 0; i < 50; i++)
	{
		b += pow(infectedInOneDay[i] - m, 2);
	}
	cout << (int)sqrt(a * b) << endl << endl;
	return 0;
}

infected generateData(int day)
{
	infected agent;
	agent.condition_of_agent = infectedAndDontKnow;
	agent.m = rand() % 50;
	agent.R = 10 - E(0.5);
	agent.HA = N(6, 0.5);
	agent.chanceToStartTreatment = 0;
	agent.A = N(AP, 5.5);
	agent.M = 0;
	agent.RT = 1 + U(20 - 1.5 * agent.HA - 0.5 * agent.M, 5 - 0.25 * agent.HA - 0.1 * agent.M);
	agent.dayOfRecovering = agent.RT + day;
	agent.DR = agent.A * 0.05 + U(15 - 1.5 * agent.HA - 0.5 * agent.M, 3 - 0.25 * agent.HA - 0.1 * agent.M);
	return agent;
}
void newInfected(vector<infected>& people, int infectedToday, int day)
{
	for (int i = 0; i < infectedToday; i++)
	{
		people.push_back(generateData(day));
	}
}

bool chance(float chance)
{
	return (rand() % 101) < chance;
}
float N(float m, float sigma)
{
	float n = 12;
	float sum = 0;
	float Ri;
	for (int i = 0; i < 12; i++)
	{
		Ri = (rand() % 100) * 0.01;
		sum += Ri;
	}
	float result = m + sigma * sqrt(12 / n) * (sum - n / 2);
	return result;
}
float U(float c, float d) {
	float a, b;
	a = c - d;
	b = c + d;
	if (a < 0)
		a = 0;
	float k;
	k = b - a;
	float result;
	result = (rand() % 100) * 0.01;
	result *= k;
	result += a;
	return result;
};
float E(float lamda)
{
	float Ri = (rand() % 100) * 0.01;
	float result = (-1 / lamda) * log(1 - Ri);
	return result;
}